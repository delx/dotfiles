# dotfiles

Configuration files for a bunch of Unix apps I use regularly.

## Basic setup

```
git clone https://code.delx.au/dotfiles .dotfiles
```


Make a backup of any existing files:
```
cd
mkdir -p backup
mv .bash .bash_profile .bashrc .gitconfig .inputrc .pythonrc.py .screenrc .vimrc backup/
```


Symlink the config files into your home directory:
```
ln -sf ~/.dotfiles/{.bash,.bash_profile,.bashrc,.gitconfig,.inputrc,.pythonrc.py,.screenrc,.vimrc} ~/
ln -sf ~/.dotfiles/.gitignore_global ~/.gitignore
ln -sf ~/.dotfiles/.ssh/config ~/.ssh/
mkdir ~/.config/environment.d && ln -sf ~/.dotfiles/.config/environment.d/* ~/.config/environment.d/
```


## Emacs setup

Clone the dotemacs repository:
```
git clone https://code.delx.au/dotemacs ~/.emacs.d
```


## Font setup

Symlink the font config:
```
mkdir -p ~/.config/fontconfig
ln -s ~/.dotfiles/.config/fontconfig/fonts.conf ~/.config/fontconfig/
```


## Firefox setup

Privacy:
```
privacy.donottrackheader.enabled = true
privacy.trackingprotection.enabled = true
```

Make the URL bar more stupid:
```
browser.fixup.alternate.enabled = false
browser.urlbar.trimURLs = false
keyword.enabled = false
```

Blank home and new tab page:
```
browser.startup.homepage = about:blank
browser.newtabpage.enabled = false
browser.newtabpage.enhanced = false
```

Send DNS through SOCKS proxy:
```
network.proxy.socks_remote_dns = true
```

Graphics acceleration, reduces tearing in video playback:
```
layers.acceleration.force-enabled = true
```

Don't prompt to reset profile if unused for a little while:
```
browser.disableResetPrompt = false
```

Open links from external apps in new window:
```
browser.link.open_newwindow.override.external = 2
```
